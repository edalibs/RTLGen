//; _mname_ << "test" << num; // set the module name to test7 (since num has been set to 7)
//; _fname_ << MNAME << ".v"; // set the file name to the module name + ".v" i.e. test7.v
module `MNAME`
//; for (INT i = 0; i < num; ++i) {
    assign dout`i`_o = din`i`_i;
//; }
endmodule

