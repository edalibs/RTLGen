**DOCUMENTATION IS A WORK IN PROGRESS !!!!!!!**

---

# RTL Generator (RTLGen)

---

## Overview
RTLGen is a code generator which allows you to generate complex output files by embedding C++ into your input file to control what appears in the output file.  Each line of the input file will appear in the output file unless it is prefixed with the special character combination `//;`.  This prefix tells the tool that that line is C++ and should be treated as executable code. All other lines appear in the output file.

For example, this:
```verilog
//; for (INT i = 0; i < 7; ++i) {
assign dout`i`_o = din`i`_i;
//; }
```
becomes this:
```verilog
assign dout0_o = din0_i;
assign dout1_o = din1_i;
assign dout2_o = din2_i;
assign dout3_o = din3_i;
assign dout4_o = din4_i;
assign dout5_o = din5_i;
assign dout6_o = din6_i;
```
RTLGen takes the input file and produces a C++ program where each line starting with `//;` is put directly into the C++ program (less the `//;` prefix), and every other line is wrapped in qoutes and appended to an output file stream as follows.
```cpp
     for (INT i = 0; i < num; ++i) {
    ofile_ss << "    assign dout" << std::setw(0) << setPadding(i) << "_o = din" << std::setw(0) << setPadding(i) << "_i;" << std::endl;
     }
```
Wherever a line contains anything enclosed in ``` ` ``` characters such as ``` `i` ``` in the above example, the contents is passed directly to the output stream. Here `i` is passed to the output stream directly (via a function `setPadding()` which does nothing in this example), whereas everything else on the line is wrapped in quotes and passed as a C string to the output stream.

---

## Language

### Types
The supported standard data types are:

| Name      | Type/Definition               |
| :---      | :---                          |
| `_mname_` | `std::stringstream`           |
| `_fname_` | `std::stringstream`           |
| `MNAME`   | `#define MNAME _mname_.str()` |
| `INT`     | `int`                         |
| `UINT`    | `unsigned`                    |
| `STR`     | `std::string`                 |
| `STRV`    | `std::vector<std::string>`    |

Avoid using standard data types other than those defined above.  User defined ADTs are fine.

### Built-in Definitions
There are three built-in definitions; `_mname_`, `_fname_`, and `MNAME`. `_mname_` and `_fname_` are variables of type `std::stringstream`. The output file name is set to `_fname_`. `_mname_` is defined for convenience only and intended to store the module name.  `MNAME` is defined as `_mname_.str()` such that it can be used to set the module name directly like so:
```verilog
module `MNAME`
```

#### STRV
Operator `<<` has been overloaded to append strings to the vector.  For example:
```cpp
//; SRTV v;
//; v << "string1" << "string2";
//; // v now contains the two strings above.
```

### Macros

There are also some standard macros:

* `ITOA(X)` - Converts `INT` `X` to a string
* `PADN(X, N)` - Returns a string of the number of spaces required to pad `STR` `X` to `N` characters.
* `PADI(X, I)` - Returns a string of the number of spaces required to pad the decimal representation of `INT` `X` to the number of characters in the decomal representation of `INT` `I`.
* `PADS(X, S)` - Returns a string of the number of spaces required to pad `STR` `X` to the length of `STR` `S`.
* `PADV(X, V)` - Returns a string of the number of spaces required to pad `STR` `X` to the longest string in `STRV` `V`.
* `CLOG2(X)`   - Returns the number of binary bits required to represent the value of `INT` `X`.

---

## A More Detailed Example

Assume that we create the file below called `example.cpp.v`. `_fname_` is defined to allow the output file name to be dependant on variables defined in the input file, and `_mname_` is defined for convenience.
```verilog
//; _mname_ << "test" << num; // set the module name to test7 (since num has been set to 7)
//; _fname_ << MNAME << ".v"; // set the file name to the module name + ".v" i.e. test7.v
module `MNAME`
//; for (INT i = 0; i < num; ++i) {
    assign dout`i`_o = din`i`_i;
//; }
endmodule
```
The for loop in the example increments `i` until it reaches `num` which is not defined in the file `example.cpp.v`. Since we want to set the maximum value of `i` without modifying the input file we're instead going to externally pass in a C++ statement defining it which we'd like to be placed in the global section of C++ program.  To do this we use the `-g` option when running RTLGen to generate the program. The `-f` option tells RTLGen what the input file is.
```bash
$ rtlgen -f example/example.cpp.v -g "INT num = 7;"
```
Running the above command produces the C++ program `out.cpp` below.  Notice how `INT num = 7;` has been dumped into the global declaration section of the program, and the lines `_mname_ << "test" << num; // set the module name to test7 (since num has been set to 7)` and `_fname_ << MNAME << ".v"; // set the file name to the module name + ".v" i.e. test7.v` have been inserted into the main function.  This allows the Verilog module name and output file name to be based on a value passed to RTLGen as an argument on the command line.

```cpp
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>

#define ITOA(X)    std::to_string(X)
#define INT        int
#define STR        std::string
#define STRV       std::vector<std::string>
#define PADN(X, N) std::string((N - ITOA(X).size()), ' ')
#define PADI(X, I) PADN(X, ITOA(I).size())
#define PADS(X, S) std::string(S.size() - X.size(), ' ')
#define PADV(X, V) std::string(maxOfStringVector(V) - X.size(), ' ')

std::stringstream _mname_;
std::stringstream _fname_;

#define MNAME      _mname_.str()

INT num = 7;


std::string _padding;

unsigned const setPadding(unsigned const value) {
    return value;
}
int const setPadding(int const value) {
    return value;
}
std::string const setPadding(std::string const &str) {
    unsigned space_count = 0;

    _padding = "";
    for (int i = str.size() - 1; i >= 0 && str[i] == ' '; --i) {
        _padding += ' ';
    }

    return str;
}

unsigned const maxOfStringVector(std::vector<std::string> v) {
    unsigned max = 0;
    for (unsigned i = 0; i < v.size(); ++i) {
        max = v[i].size() > max ? v[i].size() : max;
    }
    return max;
}

std::vector<std::string> &operator << (std::vector<std::string> &v, std::string s) {
    v.push_back(s);
    return v;
}

int main() {
    std::ofstream ofile;
    std::stringstream ofile_ss;
     _mname_ << "test" << num; // set the module name to test7 (since num has been set to 7)
     _fname_ << MNAME << ".v"; // set the file name to the module name + ".v" i.e. test7.v
    ofile_ss << "module " << std::setw(0) << setPadding(MNAME) << "" << std::endl;
     for (INT i = 0; i < num; ++i) {
    ofile_ss << "    assign dout" << std::setw(0) << setPadding(i) << "_o = din" << std::setw(0) << setPadding(i) << "_i;" << std::endl;
     }
    ofile_ss << "endmodule" << std::endl;
    ofile_ss << "" << std::endl;
    if (_fname_.str().empty()) {
        std::cout << "ERROR: Bad file name '" << _fname_.str() << "'.  'std::stringstream _fname_' must not be empty." << std::endl;
        return 1;
    }
    ofile.open(_fname_.str());
    if (!ofile.is_open()) {
        std::cout << "ERROR: Cannot open file '" << _fname_.str() << "' for output." << std::endl;
        return 1;
    }
    ofile << ofile_ss.str();
    ofile.close();
    return 0;
}
```
Compiling and running the program like so:
```bash
$ g++ out.cpp -o out; ./out
```
Yields `test7.v` below.
```verilog
module test7
    assign dout0_o = din0_i;
    assign dout1_o = din1_i;
    assign dout2_o = din2_i;
    assign dout3_o = din3_i;
    assign dout4_o = din4_i;
    assign dout5_o = din5_i;
    assign dout6_o = din6_i;
endmodule
```

