#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <getopt.h>

void insertEscapes(std::string &str) {
    std::string temp;

    for (unsigned i = 0; i < str.size(); ++i) {
        if (str[i] == '\"') {
            temp += "\\";
        }
        temp += str[i];
    }
    str = temp;
}
unsigned const countLeadingSpaces(std::string const &str) {
    unsigned space_count = 0;

    for (unsigned i = 0; i < str.size() && str[i] == ' '; ++i) {
        space_count += 1;
    }

    return space_count;
}
/*
unsigned const countTrailingSpaces(std::string const &str) {
    unsigned space_count = 0;

    for (int i = str.size() - 1; i >= 0 && str[i] == ' '; --i) {
        space_count += 1;
    }

    return space_count;
}
std::string const padding(int const size) {
    std::string padding;

    for (int i = 0; i < size; ++i) {
        padding += ' ';
    }

    return padding;
}
*/

void print_help() {
    std::cout << "Usage:" << std::endl;
    std::cout << "    $rtlgen -f <input file> [-i <optional header file>] [-g <optional global c++ code>] [-o <optional output file name>]"
              << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        print_help();
        return 1;
    }

    std::string ifile_name;
    std::string ofile_name;
    std::string hfile_name;
    std::string global_string;

    for (int i = 0; (i = getopt(argc, argv, "hf:i:g:o:")) != -1; ) {
        switch (i) {
            case 'f':
                ifile_name = std::string(optarg);
                break;
            case 'o':
                ofile_name = std::string(optarg);
                break;
            case 'i':
                hfile_name = std::string(optarg);
                break;
            case 'g':
                global_string = std::string(optarg);
                break;
            case 'h':
                print_help();
                return 1;
            default:
                print_help();
                return 1;
        }
    }

    if (ifile_name.empty()) {
        std::cout << "ERROR: Input file not specified." << std::endl;
        return 1;
    }

    if (ofile_name.empty()) {
        ofile_name = "out.cpp";
        std::cout << "INFO: Onput file name not specified, defaulting to '" << ofile_name << "'." << std::endl;
    }

/*
    std::string mname;
    {
        size_t lastdot = ofile_name.find_last_of(".");
        mname = ofile_name.substr(0, lastdot);

        size_t lastindex = mname.find_last_of("/");
        if (lastindex != std::string::npos) {
            mname = mname.substr(lastindex + 1, std::string::npos);
        }
    }
*/

    std::ifstream ifile;
    std::ofstream ofile;

    ifile.open(ifile_name.c_str());
    if (!ifile.is_open()) {
        std::cout << "ERROR: Cannot open file '" << ifile_name << "'." << std::endl;
        return 1;
    }

    ofile.open(ofile_name.c_str());
    if (!ofile.is_open()) {
        ifile.close();
        std::cout << "ERROR: Cannot open file '" << ofile_name << "'." << std::endl;
        return 1;
    }

    std::string line;

    ofile << "#include <iostream>" << std::endl;
    ofile << "#include <fstream>" << std::endl;
    ofile << "#include <iomanip>" << std::endl;
    ofile << "#include <vector>" << std::endl;
    ofile << "#include <string>" << std::endl;
    ofile << "#include <sstream>" << std::endl;
    ofile << "" << std::endl;
    ofile << "#define ITOA(X)    std::to_string(X)" << std::endl;
    ofile << "#define INT        int" << std::endl;
    ofile << "#define UINT       unsigned" << std::endl;
    ofile << "#define STR        std::string" << std::endl;
    ofile << "#define STRV       std::vector<std::string>" << std::endl;
    ofile << "#define PADN(X, N) std::string((N - ITOA(X).size()), ' ')" << std::endl;
    ofile << "#define PADI(X, I) PADN(X, ITOA(I).size())" << std::endl;
    ofile << "#define PADS(X, S) std::string(S.size() - X.size(), ' ')" << std::endl;
    ofile << "#define PADV(X, V) std::string(maxOfStringVector(V) - X.size(), ' ')" << std::endl;
    ofile << "#define CLOG2(X)   clog2(X)" << std::endl;
    ofile << "" << std::endl;
    ofile << "std::stringstream _mname_;" << std::endl;
    ofile << "std::stringstream _fname_;" << std::endl;
    ofile << "" << std::endl;
    ofile << "#define MNAME      _mname_.str()" << std::endl;
    ofile << "" << std::endl;
    if (!global_string.empty()) {
        ofile << global_string << std::endl;
    }
    ofile << "" << std::endl;
    if (!hfile_name.empty()) {
        ofile << "#include \"" << hfile_name << "\"" << std::endl;
    }
    ofile << "" << std::endl;
    ofile << "std::string _padding;" << std::endl;
    ofile << "" << std::endl;
    ofile << "unsigned const setPadding(unsigned const value) {" << std::endl;
    ofile << "    return value;" << std::endl;
    ofile << "}" << std::endl;
    ofile << "int const setPadding(int const value) {" << std::endl;
    ofile << "    return value;" << std::endl;
    ofile << "}" << std::endl;
    ofile << "std::string const setPadding(std::string const &str) {" << std::endl;
    ofile << "    unsigned space_count = 0;" << std::endl;
    ofile << "" << std::endl;
    ofile << "    _padding = \"\";" << std::endl;
    ofile << "    for (int i = str.size() - 1; i >= 0 && str[i] == ' '; --i) {" << std::endl;
    ofile << "        _padding += ' ';" << std::endl;
    ofile << "    }" << std::endl;
    ofile << "" << std::endl;
    ofile << "    return str;" << std::endl;
    ofile << "}" << std::endl;
    ofile << "" << std::endl;
    ofile << "unsigned const maxOfStringVector(std::vector<std::string> v) {" << std::endl;
    ofile << "    unsigned max = 0;" << std::endl;
    ofile << "    for (unsigned i = 0; i < v.size(); ++i) {" << std::endl;
    ofile << "        max = v[i].size() > max ? v[i].size() : max;" << std::endl;
    ofile << "    }" << std::endl;
    ofile << "    return max;" << std::endl;
    ofile << "}" << std::endl;
    ofile << "" << std::endl;
    ofile << "std::vector<std::string> &operator << (std::vector<std::string> &v, std::string s) {" << std::endl;
    ofile << "    v.push_back(s);" << std::endl;
    ofile << "    return v;" << std::endl;
    ofile << "}" << std::endl;
    ofile << "" << std::endl;
    ofile << "INT const clog2(INT x) {";
    ofile << "    INT y = 0;";
    ofile << "    for (; x != 0; ++y) {";
    ofile << "        x = (INT)((UINT)x >> 1);";
    ofile << "    }";
    ofile << "    return y;";
    ofile << "}";
    ofile << "" << std::endl;
    ofile << "int main() {" << std::endl;
    ofile << "    std::ofstream ofile;" << std::endl;
    ofile << "    std::stringstream ofile_ss;" << std::endl;
    while (std::getline(ifile, line)) {
        if (line.find("//;") == std::string::npos) { // standard RTL line
            insertEscapes(line);

            size_t start;
            std::string subline = line;
            bool quotes = true;
            
            ofile << "    ofile_ss";

            while ((start = subline.find("`")) != std::string::npos) {
                std::string workline = subline.substr(0, start);
                //std::cout << subline << ":" << start << ":" << workline << std::endl;
                if (!quotes) { // inserting C++ variable
                    if (workline.empty()) { // apply padding
                        workline = "_padding";
                    } else { // insert workline
                        ofile << " << std::setw(" << countLeadingSpaces(workline) << ")";
                        workline = "setPadding(" + (workline) + ")";
                    }
                }
                ofile << " <<" << (quotes ? " \"" : " ") << (workline) << (quotes ? "\"" : "");
                subline = subline.substr(start + 1, std::string::npos);
                quotes = !quotes;
            }
            ofile << (quotes ? " << \"" : "") << subline << (quotes ? "\"" : "") << " << std::endl;" << std::endl;
        } else {
            ofile << "    " << line.substr(line.find("//;") + 3, std::string::npos) << std::endl;
        }
    }
    ofile << "    if (_fname_.str().empty()) {" << std::endl;
    ofile << "        std::cout << \"ERROR: Bad file name '\" << _fname_.str() << \"'.  'std::stringstream _fname_' must not be empty.\" << std::endl;" << std::endl;
    ofile << "        return 1;" << std::endl;
    ofile << "    }" << std::endl;
    ofile << "    ofile.open(_fname_.str());" << std::endl;
    ofile << "    if (!ofile.is_open()) {" << std::endl;
    ofile << "        std::cout << \"ERROR: Cannot open file '\" << _fname_.str() << \"' for output.\" << std::endl;" << std::endl;
    ofile << "        return 1;" << std::endl;
    ofile << "    }" << std::endl;
    ofile << "    ofile << ofile_ss.str();" << std::endl;
    ofile << "    ofile.close();" << std::endl;
    ofile << "    return 0;" << std::endl;
    ofile << "}" << std::endl;

    ifile.close();
    ofile.close();

    return 0;
}
